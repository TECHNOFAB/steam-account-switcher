[![pipeline status](https://gitlab.com/TECHNOFAB/steam-account-switcher/badges/master/pipeline.svg?key_text=Pipeline)]()
[![made with svelte](https://img.shields.io/badge/Made%20with-Rust-dea584.svg?logo=rust&logoColor=white)](https://www.rust-lang.org/)

# Steam Account Switcher

Simple account switcher for Steam on Linux. Uses ksni to register a tray applet on KDE and freedesktop. This changes the
username in the Steam registry to login as another user and restarts Steam.

## Download

See [releases](https://gitlab.com/TECHNOFAB/steam-account-switcher/-/releases)
for the binary download

## Installing

```shell
wget <latest binary> -O /usr/local/bin/steam-account-switcher
chmod +x /usr/local/bin/steam-account-switcher
```

## Systemd service

Simple user service for systemd

```shell
# ~/.local/share/systemd/user/steam-account-switcher.service
[Unit]
Description=Steam Account Switcher

[Service]
Type=simple
ExecStart=/usr/local/bin/steam-account-switcher

[Install]
WantedBy=default.target
```

Enable this with `systemctl --user enable steam-account-switcher`
and start it with `systemctl --user start steam-account-switcher`.
