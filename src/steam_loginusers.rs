use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, PartialEq)]
#[serde(rename_all = "PascalCase")]
pub(crate) struct SteamUserData {
    pub(crate) account_name: String,
    pub(crate) persona_name: String,
    pub(crate) remember_password: bool,
    pub(crate) most_recent: bool,
    pub(crate) allow_auto_login: bool,
    pub(crate) timestamp: usize,
    #[serde(default)]
    pub(crate) wants_offline_mode: bool,
    pub(crate) skip_offline_mode_warning: bool,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
#[serde(rename = "users")]
pub(crate) struct SteamLoginUsers(pub(crate) HashMap<String, SteamUserData>);
