use std::fs;
use std::process::{Command, Stdio};
use std::thread::sleep;
use std::time::Duration;

use ksni;
use ksni::menu::RadioItem;
use nix::errno::Errno::ESRCH;
use nix::sys::signal;
use nix::unistd::Pid;

use crate::steam_loginusers::SteamLoginUsers;
use crate::steam_registry::{extract_user, insert_user};

mod steam_loginusers;
mod steam_registry;

struct SteamAccountSwitcherTray {
    selected_option: usize,
    buttons: Vec<RadioItem>,
    usernames: Vec<String>,
}

impl ksni::Tray for SteamAccountSwitcherTray {
    fn title(&self) -> String {
        "Steam Account Switcher".into()
    }
    fn icon_name(&self) -> String {
        "user".into()
    }
    fn menu(&self) -> Vec<ksni::MenuItem<Self>> {
        use ksni::menu::*;
        vec![
            RadioGroup {
                selected: self.selected_option,
                select: Box::new(|this: &mut Self, current| {
                    this.selected_option = current;
                    let username = &this.usernames[current];
                    println!("clicked on {}", username);
                    let success = stop_steam();
                    write_current_user(username);
                    if success {
                        std::thread::spawn(start_steam);
                    } else {
                        println!("Steam is still running, please restart it yourself!");
                    }
                }),
                options: self.buttons.iter().map(|item| RadioItem {
                    label: item.label.clone(),
                    enabled: item.enabled.clone(),
                    visible: item.visible.clone(),
                    icon_name: item.icon_name.clone(),
                    icon_data: item.icon_data.clone(),
                    shortcut: item.shortcut.clone(),
                    disposition: item.disposition.clone(),
                }).collect::<Vec<_>>(),
                ..Default::default()
            }
                .into(),
            StandardItem {
                label: "Exit".into(),
                icon_name: "application-exit".into(),
                activate: Box::new(|_| std::process::exit(0)),
                ..Default::default()
            }
                .into(),
        ]
    }
}

fn load_file(filename: String) -> String {
    return fs::read_to_string(&filename)
        .expect(&format!("Something went wrong reading the file {}", &filename));
}

fn load_current_user() -> String {
    let data = load_file(dirs::home_dir().unwrap().join(".steam/registry.vdf").into_os_string().into_string().unwrap());
    return extract_user(&data);
}

fn load_users() -> SteamLoginUsers {
    let data = load_file(dirs::data_local_dir().unwrap().join("Steam/config/loginusers.vdf").into_os_string().into_string().unwrap());
    return vdf_serde::from_str::<SteamLoginUsers>(&data).unwrap();
}

fn backup_registry() {
    let original = dirs::home_dir().unwrap().join(".steam/registry.vdf").into_os_string().into_string().unwrap();
    let backup = dirs::home_dir().unwrap().join(".steam/registry.vdf.bak").into_os_string().into_string().unwrap();
    fs::copy(original, backup).expect("Could not back up registry.vdf!");
}

fn write_current_user(username: &String) {
    let file = dirs::home_dir().unwrap().join(".steam/registry.vdf").into_os_string().into_string().unwrap();
    let data = load_file(file.clone());
    let res = insert_user(&data, username);
    println!("Writing user: {}", username);
    backup_registry();
    fs::write(file, res).expect("Unable to write file");
}

fn stop_steam() -> bool {
    let mut data = load_file(dirs::home_dir().unwrap().join(".steam/steam.pid").into_os_string().into_string().unwrap());
    data.pop();
    let pid = data.parse::<i32>().unwrap();
    return match signal::kill(Pid::from_raw(pid), signal::Signal::SIGTERM) {
        Ok(_val) => {
            println!("Steam stopped.");
            true
        },
        Err(e) => {
            println!("Could not stop steam! {:?}", e);
            if e == nix::Error::Sys(ESRCH) {
                return true;
            }
            false
        }
    }
}

fn start_steam() {
    println!("Waiting 5s, then starting Steam.");
    sleep(Duration::new(5, 0));

    println!("Starting steam...");
    let status = Command::new("/usr/bin/steam")
        .arg("&")
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .status()
        .expect("failed to start steam");

    if !status.success() {
        println!("Could not start steam, please start it yourself!");
    } else {
        println!("Started!");
    }
}

fn main() {
    let users = load_users();
    let current_user = load_current_user();

    let mut radio_buttons: Vec<RadioItem> = Vec::new();
    let mut usernames: Vec<String> = Vec::new();
    for (_, user_data) in users.0 {
        usernames.push(user_data.account_name.clone());
        radio_buttons.push(
            RadioItem {
                label: user_data.account_name.into(),
                icon_name: "user".into(),
                ..Default::default()
            });
    }

    println!("{}, all users: {:?}", current_user, usernames);

    let service = ksni::TrayService::new(SteamAccountSwitcherTray {
        selected_option: usernames.iter().position(|r| r.eq(&current_user)).unwrap(),
        buttons: radio_buttons,
        usernames,
    });
    service.spawn();

    // Run forever
    loop {
        std::thread::park();
    }
}
