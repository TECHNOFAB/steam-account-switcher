use std::process::exit;

use regex::{Captures, Regex};

fn get_re() -> Regex {
    return Regex::new(r#"("AutoLoginUser"(?: +|	+)"(?P<username>.+)")"#).unwrap();
}

fn get_re_captures(content: &String) -> Captures {
    let re = get_re();
    if !re.is_match(content) {
        println!("Failed to parse current user");
        exit(1);
    }
    return re.captures(content).unwrap();
}

pub(crate) fn extract_user(content: &String) -> String {
    let caps = get_re_captures(content);
    return caps.get(2).map_or("".to_string(), |m| m.as_str().to_string());
}

pub(crate) fn insert_user(content: &String, username: &String) -> String {
    let re = get_re();
    let caps = get_re_captures(content);
    let replacement = caps.get(1).map_or("".to_string(), |m| m.as_str().to_string());
    let old_username = caps.get(2).map_or("".to_string(), |m| m.as_str().to_string());
    let new = replacement.replace(&old_username, &username).to_string();
    return re.replace_all(content, new).to_string();
}
